﻿using System;

namespace Kodune3
{
    internal class Program
    {
        private static bool _gameIsRunning = true;
        private static int _guessTrys = 0;
        private static int _numberToGuess = new Random().Next(1, 50);
        
        public static void Main(string[] args)
        {
            // For testing:
            // Console.WriteLine(numberToGuess);
            
            Console.WriteLine("Püüa ära arvata number, mille ma välja mõtlesin vahemikus 1 - 50 (lihtsalt Enter lõpetab mängu) :)");
            while (_gameIsRunning)
            {
                var input = Console.ReadLine();
                if (input == "")
                {
                    exitGame();
                }
                else
                {
                    try
                    {
                        var number = Convert.ToInt32(input);
                        _guessTrys++;
                        if (number > _numberToGuess)
                        {
                            Console.WriteLine("Sinu sisestatud number on liiga suur!");
                        }
                        else if (number < _numberToGuess)
                        {
                            Console.WriteLine("Sinu sisestatud number on liiga väike!");
                        }
                        else
                        {
                            exitGame(true);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Sisestada võib ainult numbreid, lõpetamiseks vajuta lihtsalt Enter klahvi");
                    }
                }
            }
        }

        private static void exitGame(bool youGuessedIt = false)
        {
            if (youGuessedIt)
            {
                Console.WriteLine($"Võitsid mängu, arvasid {_guessTrys} korda.");
            }
            else
            {
                Console.WriteLine($"Mäng läbi, kahjuks sel korral õiget numbrit ära ei arvanud, kokku arvasid {_guessTrys} korda.");
                Console.WriteLine($"Õige number oli {_numberToGuess}.");
            }
            _gameIsRunning = false;
        }
        
        
        
        
        
    }
}