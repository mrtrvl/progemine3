﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace Kodune4
{
    internal class Program
    {
        private static bool _gameIsRunning = true;
        private static string _defaultFieldTile = "#";
        private static bool _playerOne = true;
        private static string _mark;
        private static string _playerName;

        private static readonly string[,] _field = new string[3, 3]
        {
            {_defaultFieldTile, _defaultFieldTile, _defaultFieldTile},
            {_defaultFieldTile, _defaultFieldTile, _defaultFieldTile},
            {_defaultFieldTile, _defaultFieldTile, _defaultFieldTile}
        };
        
        public static void Main(string[] args)
        {
            while (_gameIsRunning)
            {
                DrawField();

                if (_playerOne)
                {
                    _mark = "X";
                    _playerName = "Mängija1";
                }
                else
                {
                    _mark = "O";
                    _playerName = "Mängija2";
                }
                
                Console.Write($"{_playerName}, sisesta oma käigu koordinaadid (rida,tulp): ");
                
                var input = Console.ReadLine();
                
                try
                {
                    var row = Convert.ToInt32(input.Split(',')[0]) - 1;     // Kuna väljaku indeksid näidatakse number 1st,
                    var column = Convert.ToInt32(input.Split(',')[1]) - 1;  // tuleb 1 maha lahutada
                    
                    if ((row >= 0 && row <= 2) && (column >= 0 && column <= 2)) // !!Massiivi indeksid algavad 0st :)
                    {
                        MakeMove(row, column);
                        if (CheckForWin(_mark))
                        {
                            GameOver();
                        }
                    }
                    else
                    {
                        Console.WriteLine("Koordinaadid peavad jääma 1 ja 3 vahele!");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Kontrolli, et sisestatud koordinaadid oleksid korrektsed!");
                }
            }
        }
        
        private static void DrawField()
        {
            Console.WriteLine();
            Console.WriteLine("  1 2 3"); // Tulpade indeksid
            for (int i = 0; i < 3; i++)
            {
                var row = $"{i + 1}";
                for (int j = 0; j < 3; j++)
                {
                    row += $" {_field[i, j]}";
                }
                Console.WriteLine(row);
            }
            Console.WriteLine();
        }

        private static void MakeMove(int row, int column)
        {
            if (CheckIfMoveIsValid(row, column))
            {
                _field[row, column] = _mark;
                _playerOne = !_playerOne;
            }
            else
            {
                Console.WriteLine("Ei saa käia väljale, kuhu on juba käidud!");
            }
        }

        private static bool CheckIfMoveIsValid(int row, int column)
        {
            return _field[row, column] == _defaultFieldTile;
        }

        private static bool CheckForWin(string tile)
        {
            for (int i = 0; i < 3; i++) // Read ja tulbad
            {
                if (_field[i, 0] == tile && _field[i, 1] == tile && _field[i, 2] == tile)
                {
                    return true;
                }
                
                if (_field[0, i] == tile && _field[1, i] == tile && _field[2, i] == tile)
                {
                    return true;
                }
            }
            
            // Diagonaalid
            
            if (_field[0, 0] == tile && _field[1, 1] == tile && _field[2, 2] == tile)
            {
                return true;
            }
            
            if (_field[0, 2] == tile && _field[1, 1] == tile && _field[2, 0] == tile)
            {
                return true;
            }
            
            return false;
        }

        private static void GameOver()
        {    
            DrawField();
            Console.WriteLine($"Palju õnne, {_playerName}, oled võitnud!!!");
            _gameIsRunning = false;
        }
    }
}