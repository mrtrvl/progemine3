﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Kodune2
{
    internal class Program
    {
        static List<int> numbers = new List<int>();
        public static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Sisesta mingi täisarv (Enter klahv lõpetab): ");
                
                var input = Console.ReadLine();
                
                if (input == "")
                {
                    if (numbers.Count > 0)
                    {
                        Console.WriteLine($"Väikseim sisestatud number: {MinValueFromList(numbers)}");
                        Console.WriteLine($"Suurim sisestatud number: {MaxValueFromList(numbers)}");
                        Console.WriteLine($"Sisestatud numbrite keskmine: {AverageValueFromList(numbers)}");
                        Console.WriteLine($"Sisestatud numbrite summa: {SumOfList(numbers)}");  
                    }
                    else
                    {
                        Console.WriteLine("Paistab, et ei sisestatud ühtegi numbrit, mida arvutada :(");
                    }
                    break;
                }
                else
                {
                    try
                    {
                        var number = Convert.ToInt32(input);
                        numbers.Add(number);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Sisestada võib ainult numbreid, lõpetamiseks vajuta lihtsalt Enter klahvi");
                    }
                }
            }
        }

        private static int MinValueFromList(List<int> numbers)
        {
            return numbers.Min();
        }

        private static int MaxValueFromList(List<int> numbers)
        {
            return numbers.Max();
        }

        private static float AverageValueFromList(List<int> numbers)
        {
            
            var average = (float)SumOfList(numbers) / (float)numbers.Count;
            
            return average;
        }

        private static int SumOfList(List<int> numbers)
        {
            var sum = 0;
            
            foreach (var number in numbers)
            {
                sum += number;
            }

            return sum;
        }
    }
}